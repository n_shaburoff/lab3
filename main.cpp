#include <SFML/Graphics.hpp>
#include <vector>

int main()
{
	sf::RenderWindow myWindow;     
	myWindow.create(sf::VideoMode(800,600), "Lab3");
	sf::CircleShape shape;
	shape.setFillColor(sf::Color::Green);
	shape.setRadius(10);
	sf::Event event;
	while (true)
	{
		while (myWindow.pollEvent(event))
		{
			if (event.type == sf::Event::Closed)
				myWindow.close();
			if (event.type == sf::Event::MouseButtonPressed && event.mouseButton.button == sf::Mouse::Left) {
				shape.setPosition(event.mouseButton.x, event.mouseButton.y);
			}
		}
		myWindow.clear();
		myWindow.draw(shape);
		myWindow.display();
	}

	return 0;
}