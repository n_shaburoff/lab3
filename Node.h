#pragma once
#include <string>
#include <vector>
#include <cmath>
class Node
{
public:
	Node();
	Node(float x, float y, std::string name = "");
	Node(Node* node);
	float x();
	float y();
	std::string name();
	void setX(float x);
	void setY(float y);
	void setName(std::string name);
	void addNeighbor(Node* node);
	std::vector<Node*> neighbors();
	float distance(Node* neighbor);
private:
	float x_;
	float y_;
	std::string name_;
	std::vector<Node*> neighbors_;
};